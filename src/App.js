import React,{useState} from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import {Container}  from 'react-bootstrap';
import { UserProvider } from './UserContext';

//For Routes
import {BrowserRouter as Router, Routes, Route}  from 'react-router-dom';

function App() {
  
 const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
    })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user, setUser,unsetUser}}>
    <Router>
     <AppNavbar/>
      <Container>
        <Routes>
          <Route path ="/" element={<Home/>}/>
          <Route path ="/courses" element={<Courses/>}/>
          <Route path ="/register" element={<Register/>}/>
          <Route path ="/login" element={<Login/>}/>    
          <Route path ="/logout" element={<Logout/>}/>
         <Route path ="*" element={<NotFound/>}/>   
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
