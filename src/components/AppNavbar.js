import React,{useState,useContext} from 'react';
import {Navbar,Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

export default function AppNavbar(){
	const {user} = useContext(UserContext);
	
	
	return(
			<Navbar bg="dark" expand="lg" variant="dark" className="mb-4">
				<Navbar.Brand as={Link} to="/" className="ms-3">Zuit Booking</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" className="me-2"></Navbar.Toggle>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link as={Link} to="/"  className="ms-3">Home</Nav.Link>
						<Nav.Link as={Link} to="/courses"  className="ms-3">Courses</Nav.Link>
				
						{(user.accessToken !==null) ?
							<Nav.Link as={Link} to="/logout" className="ms-3 me-4">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={Link} to="/login" className="ms-3 ">Login</Nav.Link>
								<Nav.Link as={Link} to="/register" className="ms-3 me-4">Register</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}