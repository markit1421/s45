import {Row,Col, Button} from 'react-bootstrap';

export default function AppBanner(){
	return(
			<Row>
				<Col className="py-5">
					<h1 className="mb-3">Batch-165 Zuitt Coding Bootcamp</h1>
					<p className="my-3">Opportunities for everyone, everywhere</p>
					<Button variant="primary" >Enroll now!</Button>
				</Col>
			</Row>
		)
}