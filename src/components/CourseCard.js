import React,{useState,useEffect} from 'react';
import {Card,Button} from 'react-bootstrap';
import PropTypes from 'prop-types';


export default function CourseCard({courseProp}){

	const {name, description, price} = courseProp;

	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);

	const [isOpen, setIsOpen] = useState(true)

	const enroll = () =>{
			setCount(count+1);
			setSeat(seat-1);
	}

	useEffect(()=>{
		if(seat ===0){
			setIsOpen(false);
		}
	},[seat])

	return(	
		<Card className="cardHighlight p-2 my-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Card.Subtitle>Enrollees:</Card.Subtitle>
				<Card.Text>{count} enrollees</Card.Text>
				<Card.Subtitle>Seats Available:</Card.Subtitle>
				<Card.Text>{seat} </Card.Text>
				{isOpen ?
					<Button variant="primary" onClick={enroll}>Enroll</Button>
					:
					<Button variant="primary" disabled>Enroll</Button>
				}
				
			</Card.Body>	
		</Card>	
		)
}

//Check if the CourseCard component is getting the correct prop types
//PropTypes are used for validationg information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
CourseCard.propTypes = {
	//shape() method it is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}


