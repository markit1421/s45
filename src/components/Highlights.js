import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Learn from Home</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae voluptatum aspernatur et possimus doloremque, distinctio, temporibus. Fugiat dolor, corrupti repudiandae, consequuntur saepe distinctio? Ad provident eius nemo reprehenderit sit alias!		
							</Card.Text>	
						</Card.Body>	
					</Card>	
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3 ">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Assumenda, eligendi sed eos nesciunt corrupti temporibus quasi ex incidunt doloremque cum exercitationem, illo quidem, laborum facilis totam nostrum vitae repellat! Harum.  
							</Card.Text>	
						</Card.Body>	
					</Card>	
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Et itaque officia tempore enim provident, nihil necessitatibus doloremque veniam, a quo quaerat! Blanditiis, minima esse, minus enim facilis ipsa officia perspiciatis!  
							</Card.Text>	
						</Card.Body>	
					</Card>	
				</Col>
			</Row>	

		)
}