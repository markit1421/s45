const coursesData = [
	{
		id: "wdc001",
		name:"PHP-Laravel",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Totam, dolor. Possimus maxime, blanditiis nobis molestiae odit veniam deserunt distinctio, laborum unde repudiandae laboriosam corporis sequi numquam iusto inventore atque? Cumque.",
		price: "45000",
		onOffer: true
	},
	{
		id: "wdc002",
		name:"Python - Django",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Totam, dolor. Possimus maxime, blanditiis nobis molestiae odit veniam deserunt distinctio, laborum unde repudiandae laboriosam corporis sequi numquam iusto inventore atque? Cumque.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name:"Java - Springboot",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Totam, dolor. Possimus maxime, blanditiis nobis molestiae odit veniam deserunt distinctio, laborum unde repudiandae laboriosam corporis sequi numquam iusto inventore atque? Cumque.",
		price: 55000,
		onOffer: true
	}
]

export default coursesData;