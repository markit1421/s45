import AppBanner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
	return(
			<>
				<AppBanner/>
	        	<Highlights/>
			</>
		)
}