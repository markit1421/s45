import React,{useState,useEffect,useContext} from 'react';
import {Form,Button,Row,Col} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';

export default function Login(){
	const navigate = useNavigate();

	const {user,setUser}=useContext(UserContext);

	const [email, setEmail]=useState('');
	const [password, setPassword]=useState('');
	
	const [isActive, setIsActive]=useState(true);
	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if(email !== '' && password !== ''){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [email, password])


	
	function authenticate(e) {
		e.preventDefault();

		fetch('http://localhost:8080/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.accessToken!==undefined){
				localStorage.setItem('accessToken',data.accessToken);
				setUser({accessToken: data.accessToken})
				Swal.fire({
				 	icon: 'success',
				  title: 'Login successful',
				  showConfirmButton: false,
				  timer: 1500
				})

				fetch('http://localhost:8080/users/userDetails', {
					headers:{
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result);
					if(result.isAdmin===true){
						localStorage.setItem('email',result.email)
						localStorage.setItem('isAdmin',result.isAdmin)
						setUser({
							email:result.email,
							isAdmin: result.isAdmin
						})

						navigate('/courses');
					}else{
						navigate('/');
					}
				})
			} else{
				Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Something went wrong.Check your credentials'
				})
			}
			setEmail('');
			setPassword('');

		})
	}


	return (
		(user.accessToken !== null) ?
			<Navigate to="/courses" />
			:
		<Row className="justify-content-md-center">
			<Col xs={12} md={6} lg={4}>
				<Form onSubmit={(e) => authenticate(e)} className="my-4">
					<h2 className="mb-3 text-center">Please Sign-in</h2>
						<Form.Group className="mb-3">
							<Form.Label>Email Address</Form.Label>
							<Form.Control
								type="email"
								placeholder="Enter Email"
								required
								value={email}
								onChange={e => setEmail(e.target.value)}
							/>
							
						</Form.Group>

						<Form.Group className="mb-3">
							<Form.Label>Password</Form.Label>
							<Form.Control
								type="password"
								placeholder="Enter Password"
								required
								value={password}
								onChange={e => setPassword(e.target.value)}
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicCheckbox">
					    	<Form.Check type="checkbox" label="Remember me" />
					    </Form.Group>	

						{isActive ?
							<Button variant="primary" type="submit" className="w-100 btn btn-lg mb-3">Login</Button>
							:
							<Button variant="primary" type="submit" className="w-100 btn btn-lg mb-3" disabled>Login</Button>
						}
					
					</Form>
				</Col>
			</Row>
		)
}